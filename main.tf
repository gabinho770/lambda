resource "aws_iam_role" "iam_for_lambda122" {
  name = "iam_for_lamb"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}

EOF

}
resource "aws_iam_role_policy" "my-policy" {
 name = "my-policy"
 role = "${aws_iam_role.iam_for_lambda122.id}"


 
 policy = <<-EOF
 {
   "Version": "2012-10-17",
   "Statement": [
     {
       "Sid": "AccessObject",
       "Effect": "Allow",
       "Action": [
         "s3:*"
       ],
      "Resource": [
        "arn:aws:s3:::terraform-bucket770",
         "arn:aws:s3:::terraform-bucket770/*"
      ]
     }
   ]
 }
EOF
}
resource "aws_s3_bucket" "bucket770" {
   bucket = "terraform-bucket770"
   
   
}
resource "aws_lambda_function" "test_lambda" {
  
  function_name = "hello"
 
  filename      =  "hello.zip"
  source_code_hash = filebase64sha256("hello.zip")
  role          =  aws_iam_role.iam_for_lambda122.arn
  
  handler       = "hello.handler"
  runtime       = "nodejs14.x"
 // layers        =  ["arn:aws:lambda:us-west-2:553035198032:layer:git:5"]
   layers        =   ["arn:aws:lambda:us-west-2:553035198032:layer:git-lambda2:8"]
                
  

}

resource "aws_lambda_permission" "allow_bucket" {
  
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.bucket770.arn
  
}



resource "aws_s3_bucket_notification" "bucket_notification" {
   bucket = aws_s3_bucket.bucket770.id
   lambda_function {
    lambda_function_arn =aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "AWSLogs/"
    filter_suffix       = ".log"
  

  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
